package com.example;

import java.io.File;
import java.io.IOException;
import java.sql.Time;
import java.time.Clock;
import java.util.List;
import java.util.Scanner;
import java.util.Timer;
import java.util.Vector;

import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.JDOMException;
import org.jdom2.input.SAXBuilder;


public class JDomParserDemo {
    public static void main(String[] args) throws IOException {

        Nonogram nonogram1 = loadXML("id65.txt"); //0.042780276
        nonogram1.printGame();
        long startTime = System.nanoTime();
        nonogram1.Solve(true);
        long endTime = System.nanoTime();
        nonogram1.printGame();
        double timeInMills = (endTime - startTime) / 1000000.0;
        double timeInSeconds = timeInMills / 1000;
        System.out.println("This took us: " + timeInSeconds);


        Nonogram nonogram2 = loadXML("id1611.txt");
        startTime = System.nanoTime();
        while(nonogram2.isDone() == false) {
            nonogram2.unifyAllOnce();
        }
        endTime = System.nanoTime();
        timeInMills = (endTime - startTime) / 1000000.0;
        timeInSeconds = timeInMills / 1000;
        System.out.println("This took us: " + timeInSeconds);

        /*

        Nonogram nonogram1 = loadXML("id21.txt");
        nonogram1.printGame();
        int i = 0;
        float lastDonePre = -1;
        System.out.println("start solving " + nonogram1.getRowNum() + " X " + nonogram1.getColNum() + " nonogram");
        long startTime = System.nanoTime();
        while(nonogram1.isDone() == false) {
            //System.out.println("run: " + i++ + " (" + String.format("%5d",nonogram1.getColNum()*nonogram1.getRowNum() - nonogram1.howMuchLeft()) + "/" + nonogram1.getColNum()*nonogram1.getRowNum() + ") " + String.format("%3.2f", nonogram1.donePercent()) + "%");
            if(lastDonePre == nonogram1.donePercent()) {
                //System.out.println("got stack...");
                break;
            }
            lastDonePre = nonogram1.donePercent();
            nonogram1.unifyAllOnce();
            //nonogram1.printGame();
        }
        long endTime = System.nanoTime();
        System.out.println("run: " + i++ + " (" + String.format("%5d",nonogram1.getColNum()*nonogram1.getRowNum() - nonogram1.howMuchLeft()) + "/" + nonogram1.getColNum()*nonogram1.getRowNum() + ") " + String.format("%3.2f", nonogram1.donePercent()) + "%");
        nonogram1.printGame();
        double timeInMills = (endTime - startTime) / 1000000.0;
        double timeInSeconds = timeInMills / 1000;
        System.out.println("This took us: " + timeInSeconds);
        */
    }

    public static Nonogram loadXML(String fileName) {
        Nonogram nonogram = null;
        try {
            File inputFile = new File(fileName);

            SAXBuilder saxBuilder = new SAXBuilder();

            Document document = saxBuilder.build(inputFile);

            List<Element> columnsList = document.getRootElement().getChild("puzzle").getChildren("clues").get(0).getChildren();
            List<Element> rowsList    = document.getRootElement().getChild("puzzle").getChildren("clues").get(1).getChildren();



            Vector<Integer>[] cols = new Vector[columnsList.size()];
            Vector<Integer>[] rows = new Vector[rowsList.size()];

            for(int i = 0; i < cols.length; i++) {
                cols[i] = new Vector<Integer>();
                List<Element> col = columnsList.get(i).getChildren();
                for(Element num: col)
                    cols[i].add(Integer.parseInt(num.getText()));
            }

            for(int i = 0; i < rows.length; i++) {
                rows[i] = new Vector<Integer>();
                List<Element> row = rowsList.get(i).getChildren();
                for(Element num: row)
                    rows[i].add(Integer.parseInt(num.getText()));
            }
            nonogram = new Nonogram(columnsList.size(), rowsList.size(), rows, cols);

        } catch (JDOMException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return nonogram;
    }
}