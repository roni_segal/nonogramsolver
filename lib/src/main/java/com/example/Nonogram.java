package com.example;

import java.awt.Point;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;
import java.util.Vector;

/**
 * Created by ronio on 5/19/2017.
 */

public class Nonogram {
    private int _colNum;
    private int _rowNum;

    private Vector<Integer>[] _rowRules;
    private Vector<Integer>[] _colRules;

    enum Cell {
        blank,
        filled,
        locked;

        @Override
        public String toString() {
            if(this == Cell.blank) return " ";
            else if(this == Cell.filled) return "X";
            return ".";
        }
    }

    Cell[][] _grid;



    public Nonogram(int colNum, int rowNum, Vector<Integer>[] rowRules, Vector<Integer>[] colRules) {
        _rowRules = rowRules;
        _colRules = colRules;
        _colNum = colNum;
        _rowNum = rowNum;
        _grid = new Cell[rowNum][colNum];
        for(int row = 0; row < rowNum; row++) {
            for(int col = 0; col < colNum; col++) {
                _grid[row][col] = Cell.blank;
            }
        }
    }

    public Nonogram(Nonogram other) {
        _rowNum = other._rowNum;
        _colNum = other._colNum;
        _rowRules = other._rowRules;
        _colRules = other._colRules;
        _grid = new Cell[_rowNum][_colNum];
        for(int row = 0; row < _rowNum; row++) {
            for(int col = 0; col < _colNum; col++) {
                _grid[row][col] = other._grid[row][col];
            }
        }
    }

    public boolean isDone() {
        for(int i = 0; i < _grid.length; i++) {
            for(int j = 0; j < _grid[i].length; j++) {
                if(_grid[i][j] == Cell.blank)
                    return false;
            }
        }
        return true;
    }

    public int howMuchLeft() {
        int notDone = 0;
        for(int i = 0; i < _grid.length; i++) {
            for(int j = 0; j < _grid[i].length; j++) {
                if(_grid[i][j] == Cell.blank)
                    notDone++;
            }
        }
        return notDone;
    }

    public float donePercent() {
        return 100 - (howMuchLeft() * 100) / (getColNum() * getRowNum());
    }

    public int getColNum() {
        return _colNum;
    }

    public int getRowNum() {
        return _rowNum;
    }

    public Vector<Integer>[] getRowRules() {
        return _rowRules;
    }

    public Vector<Integer>[] getColRules() {
        return _colRules;
    }

    Nonogram paintRow(int start, int end, int rowIndex) {
        Nonogram result = new Nonogram(this);


        for(int i = start; i < end; i++) {
            if(result._grid[rowIndex][i] == Cell.locked) return null;
            result._grid[rowIndex][i] = Cell.filled;
        }

        return result.isValidOption()? result : null;
    }
    Nonogram paintCol(int start, int end, int colIndex) {
        Nonogram result = new Nonogram(this);


        for(int i = start; i < end; i++) {
            if(result._grid[i][colIndex] == Cell.locked) return null;
            result._grid[i][colIndex] = Cell.filled;
        }

        return result.isValidOption()? result : null;
    }
    public boolean isValidOption() {
        // check rows
        for(int i = 0; i <_rowRules.length; i++) {
            Vector<Integer> rule = _rowRules[i];
            int gridIndex = 0;
            int maxRule = 0, maxSequence = 0;
            for(Integer ruleNum : rule) maxRule = Math.max(ruleNum, maxRule);
            while(gridIndex < getColNum()) {
                while(gridIndex < getColNum() && _grid[i][gridIndex] != Cell.filled) gridIndex++;
                //Count sequence of filled
                int currSequence = 0;
                while(gridIndex < getColNum() && _grid[i][gridIndex] == Cell.filled) currSequence++;
                //compare sequence length
                maxSequence = Math.max(currSequence, maxSequence);
            }

            if(maxSequence > maxRule) return false;
        }

        //check cols
        for(int i = 0; i <_colRules.length; i++) {
            Vector<Integer> rule = _colRules[i];
            int gridIndex = 0;
            int maxRule = 0, maxSequence = 0;
            for(Integer ruleNum : rule) maxRule = Math.max(ruleNum, maxRule);
            while(gridIndex < getRowNum()) {
                while(gridIndex < getRowNum() && _grid[gridIndex][i] != Cell.filled) gridIndex++;
                //Count sequence of filled
                int currSequence = 0;
                while(gridIndex < getRowNum() && _grid[gridIndex][i] == Cell.filled) currSequence++;
                //compare sequence length
                maxSequence = Math.max(currSequence, maxSequence);
            }

            if(maxSequence > maxRule) return false;
        }
        return true;
    }

    private Vector<Cell[]> getAllOptions(Cell[] baseCase, int maxGroup) {
        return getAllOptions(baseCase, 0, maxGroup);
    }
    private Vector<Cell[]> getAllOptions(Cell[] baseCase, int index, int maxGroup) {
        while(index < baseCase.length && baseCase[index] != Cell.blank) index++;
        if (index == baseCase.length) {
            Vector<Cell[]> result = new Vector<Cell[]>();
            result.add(baseCase);
            return result;
        }

        Cell[] withFilled = new Cell[baseCase.length];
        for(int i = 0; i < baseCase.length; i++) withFilled[i] = baseCase[i];
        Cell[] withLocked = new Cell[baseCase.length];
        for(int i = 0; i < baseCase.length; i++) withLocked[i] = baseCase[i];
        withFilled[index] = Cell.filled;
        withLocked[index] = Cell.locked;

        Vector<Cell[]> result = getAllOptions(withFilled, index + 1, maxGroup);
        Vector<Cell[]> temp = getAllOptions(withLocked, index + 1, maxGroup);
        for(Cell[] item : temp) {
            result.add(item);
        }
        return result;
    }

    private boolean doesMatchRule(Cell[] row, Vector<Integer> rule) {
        Vector<Integer> groups = new Vector<>();
        int currentGroup = 0;
        //System.out.print("doseMatchRule: ");
        //for(int i = 0; i < rule.size(); i++)
            //System.out.print(" " + rule.get(i));
        for(int i = 0; i < row.length; i++) {
            if(row[i] == Cell.filled) {
                currentGroup++;
            }
            else if(currentGroup > 0) {
                groups.add(currentGroup);
                currentGroup = 0;
            }
        }
        if(currentGroup > 0) {
            groups.add(currentGroup);
        }

        if(groups.size() != rule.size()) return false;
        for(int i = 0; i < groups.size(); i++) {
            if(!groups.get(i).equals(rule.get(i))) return false;
        }
        return true;
    }
    Vector<Cell[]> filterByRule(Vector<Cell[]> options, Vector<Integer> rule) {
        Vector<Cell[]> filtered = new Vector<>();
        for(Cell[] option : options) {
            if(doesMatchRule(option, rule)) {
                filtered.add(option);
            }
        }
        return filtered;
    }
    Cell[] unifyOptions(Vector<Cell[]> options) {
        Cell[] result = options.get(0);
        for(int i = 1; i < options.size(); i++) {
            for(int j = 0; j < result.length; j++) {
                if(result[j] != Cell.blank && result[j] != options.get(i)[j])
                    result[j] = Cell.blank;
            }
        }
        return result;
    }

    public boolean Solve(boolean doLog) {
        Set<Task> tasks = new HashSet<>();
        for(int i = 0; i < getRowNum();i++) {
            tasks.add(new Task(true, i));
        }
        for(int i = 0; i < getColNum(); i++) {
            tasks.add(new Task(false, i));
        }
        Vector<Integer> diff = new Vector<>();

        Iterator<Task> iterator;
        int i = 0;
        while(!tasks.isEmpty()) {
            if(doLog) {
                System.out.println(i++ + ") tasks: " + tasks.size() + " done pre.: " + donePercent() + "%");
            }
            iterator = tasks.iterator();
            Task currTask = iterator.next();
            iterator.remove();

            diff.clear();
            if(currTask.isRow) {
                Vector<Integer> rule = _rowRules[currTask.index];

                Cell[] row = new Cell[getColNum()];
                for(int j = 0; j < row.length; j++) {
                    row[j] = _grid[currTask.index][j];
                }
                int ruleSum = 0;
                for(int j = 0; j < rule.size(); j++) ruleSum += rule.get(j);
                int numSpaces = getColNum() - ruleSum;

                Cell[] result = calculateFreebieRowSmarter(row, numSpaces, rule, 0, 0);
                for(int j = 0; j < result.length; j++) {
                    if(_grid[currTask.index][j] != result[j]) diff.add(j);
                    _grid[currTask.index][j] = result[j];
                }

            }
            else {
                Vector<Integer> rule = _colRules[currTask.index];

                Cell[] col = new Cell[getRowNum()];
                for(int j = 0; j < col.length; j++) {
                    col[j] = _grid[j][currTask.index];
                }
                int ruleSum = 0;

                for(int j = 0; j < rule.size(); j++) ruleSum += rule.get(j);
                int numSpaces = getRowNum() - ruleSum;
                Cell[] result = calculateFreebieRowSmarter(col, numSpaces, rule, 0, 0);
                for(int j = 0; j < result.length; j++) {
                    if(_grid[j][currTask.index] != result[j]) diff.add(j);
                    _grid[j][currTask.index] = result[j];
                }
            }

            for(int num : diff) {
                tasks.add(new Task(!currTask.isRow, num));
            }

        }

        return isDone();
    }

    private class Task {
        public boolean isRow;
        public int index;
        Task(boolean isRow, int index) {
            this.index = index;
            this.isRow = isRow;
        }


    }

    public void unifyAllOnce() {
        /*System.out.println("        working on the rows: ");*/
        for(int i = 0; i < _rowRules.length; i++) {
            Vector<Integer> rule = _rowRules[i];
            int maxGroup = 0;
            for(int group : rule) maxGroup = Math.max(maxGroup, group);
            Cell[] row = new Cell[getColNum()];
            for(int j = 0; j < row.length; j++) {
                row[j] = _grid[i][j];
            }
            //row = unifyOptions(filterByRule(getAllOptions(row, maxGroup), rule));
            int ruleSum = 0;
            for(int j = 0; j < rule.size(); j++) ruleSum += rule.get(j);
            int numSpaces = getColNum() - ruleSum;
            /*System.out.print( String.format("%35s", "      " + rule));
            System.out.print(":  (" + i + "/" + _rowRules.length + "): ");*/
            Cell[] result = calculateFreebieRowSmarter(row, numSpaces, rule, 0, 0);
            for(int j = 0; j < result.length; j++) {
                _grid[i][j] = result[j];
            }
            /*for(int k = 0; k < result.length; k++)
                System.out.print(result[k] + " ");
            System.out.println();*/
        }

        for(int i = 0; i < _colRules.length; i++) {
            /*System.out.println("        working on the cols: ");*/
            Vector<Integer> rule = _colRules[i];
            Cell[] col = new Cell[getRowNum()];
            int maxGroup = 0;
            for(int group : rule) maxGroup = Math.max(maxGroup, group);
            for(int j = 0; j < col.length; j++) {
                col[j] = _grid[j][i];
            }
            int ruleSum = 0;
            for(int j = 0; j < rule.size(); j++) ruleSum += rule.get(j);
            int numSpaces = getRowNum() - ruleSum;
            Cell[] result = calculateFreebieRowSmarter(col, numSpaces, rule, 0, 0);
            for(int j = 0; j < result.length; j++) {
                _grid[j][i] = result[j];
            }
            /*System.out.print("        col done. (" + i + "/" + _colRules.length + "): ");
            for(int k = 0; k < result.length; k++)
                System.out.print(result[k] + " ");
            System.out.println();*/
        }
    }
    private boolean[] calculateFreebieRow(boolean[] currState, int spacesLeft, Vector<Integer> rule, int ruleIndex, int arrIndex, boolean[] result) {
        if(ruleIndex == rule.size()) {
            result = calculateAnd(currState, result);
            return result;
        }

        int maxSpaces = spacesLeft - (rule.size() - (ruleIndex + 1));
        for(int numSpaces = 1; numSpaces <= maxSpaces; numSpaces++) {
            int newIndex = arrIndex + numSpaces;
            boolean[] newState = new boolean[currState.length];
            for (int i = 0; i < currState.length; i++) newState[i] = currState[i];
            for (int i = 0; i < rule.get(ruleIndex); i++) newState[newIndex + i] = true;
            result = calculateFreebieRow(newState, spacesLeft - numSpaces, rule, ruleIndex + 1, newIndex + rule.get(ruleIndex), result);
        }
        return result;
    }

    private Cell[] calculateFreebieRowSmarter(Cell[] currState, int spacesLeft, Vector<Integer> rule, int ruleIndex, int arrIndex) {
        if(ruleIndex == rule.size()) {
            for(int i = arrIndex; i < currState.length; i++) {
                if(currState[i] == Cell.filled) return null;
                currState[i] = Cell.locked;
            }
            //result = calculateAndSmarter(currState);
            //Cell[] result = new Cell[currState.length];
            //for(int i = 0; i < currState.length; i++) result[i] = currState[i];
            return currState;
        }

        Cell[] result = null;
        int start = 1;
        if(ruleIndex == 0) start = 0;
        int maxSpaces = spacesLeft - (rule.size() - (ruleIndex  + 1));
        for(int numSpaces = start; numSpaces <= maxSpaces; numSpaces++) {
            boolean validOption = true;
            Cell[] newState = new Cell[currState.length];
            for (int i = 0; i < currState.length; i++) newState[i] = currState[i];
            for(int i = 0; i < numSpaces; i++) {
                if(currState[arrIndex + i] == Cell.filled) {
                    validOption = false;
                    break;
                }
                newState[arrIndex + i] = Cell.locked;
            }
            if(!validOption) continue;
            int newIndex = arrIndex + numSpaces;
            // 3 1 ------
            // 3 1 XXX-X-
            // 3 1 -XXX-X
            // 3 1 --X.--
            // 3 1 XXX..X
            // 3 1 XXX.X.
            // 2 --X--
            // 2 .XX..
            // 2 ..XX.
            // 2 .-X-.
            for (int i = 0; i < rule.get(ruleIndex); i++) {
                if(currState[newIndex + i] == Cell.locked) {
                    validOption = false;
                    break;
                }
                newState[newIndex + i] = Cell.filled;
            }
            if(!validOption) continue;
            //new Cell[currState.length];

            Cell[] tempResult = calculateFreebieRowSmarter(newState, spacesLeft - numSpaces, rule, ruleIndex + 1, newIndex + rule.get(ruleIndex));
            if(tempResult != null) {
                //result = calculateAndSmarter(result, calculateFreebieRowSmarter(newState, spacesLeft - numSpaces, rule, ruleIndex + 1, newIndex + rule.get(ruleIndex)));
                if(result == null) {
                    result = tempResult;
                }
                else {
                    result = calculateAndSmarter(result, tempResult);
                }
            }
        }
        return result;
    }

    Cell[] calculateAndSmarter(Cell[] a, Cell[] b) {
        Cell[] result = new Cell[a.length];
        for(int i = 0; i < a.length; i++) {
            if (a[i] != b[i]) {
                result[i] = Cell.blank;
            }
            else {
                result[i] = a[i];
            }
        }

        return result;
    }


    boolean[] calculateAnd(boolean[] a, boolean[] b) {
        for(int i = 0; i < a.length; i++) a[i] = a[i] & b[i];
        return a;
    }

    public void paintFreebies() {
        for(int i = 0; i < _rowRules.length; i++) {
            Vector<Integer> rule = _rowRules[i];
            int ruleSum = 0;
            boolean[] row = new boolean[getColNum()];
            boolean[] startingState = new boolean[getColNum()];
            int index = 0;
            for(Integer num : rule) {
                ruleSum += num;
                while(num > 0) {
                    row[index] = true;
                    index++;
                    num--;
                }
                index++;
            }
            int numSpaces = getColNum() - ruleSum;
            row = calculateFreebieRow(startingState, numSpaces, rule, 0, 0, row);
            for(int j = 0; j < row.length; j++) {
                _grid[i][j] = (row[j] || _grid[i][j] == Cell.filled)? Cell.filled: Cell.blank;
            }
        }

        for(int i = 0; i < _colRules.length; i++) {
            Vector<Integer> rule = _colRules[i];
            int ruleSum = 0;
            boolean[] row = new boolean[getRowNum()];
            boolean[] startingState = new boolean[getRowNum()];
            int index = 0;
            for(Integer num : rule) {
                ruleSum += num;
                while(num > 0) {
                    row[index] = true;
                    index++;
                    num--;
                }
                index++;
            }
            int numSpaces = getRowNum() - ruleSum;
            row = calculateFreebieRow(startingState, numSpaces, rule, 0, 0, row);
            for(int j = 0; j < row.length; j++) {
                _grid[j][i] = (row[j] || _grid[j][i] == Cell.filled) ? Cell.filled: Cell.blank;
            }
        }
    }
    /*
        Enum axis {X, Y};
        Nonogram paintRange(start, end, axis); returns Nonogram copy if the move is valid, null otherwise

            1 4 1
            2 4 5 7
        4 2 - - - * * - - - - -
        1 1     * -
                  -
                  -
                  -
                  -
                  -
                  -

              * * * * - * * - -
              * * * * - - * * -
              * * * * - - - * *
              - * * * * - * * -
              - * * * * - - * *
              - - * * * * - * *

     */


    public void printGame() {
        int max_rows_rules = 0, max_cols_rules = 0;
        for(Vector<Integer> rule : _colRules) max_cols_rules = Math.max(max_cols_rules,rule.size() + 1);
        for(Vector<Integer> rule : _rowRules) max_rows_rules = Math.max(max_rows_rules,rule.size() + 1);
        //Printing rows padding + cols rules
        String padding = new String(new char[max_rows_rules * 3]).replace("\0", " ");
        for(int i = 0; i < max_cols_rules; i++) {
            System.out.print(padding);
            for(int j = 0; j < _colRules.length; j++) {
                Vector<Integer> rule = _colRules[j];
                if(rule.size() >= (max_cols_rules - i)) {
                    String formatted = String.format("%-2d ", rule.get(i - (max_cols_rules - rule.size())));
                    System.out.print(formatted);
                }
                else {
                    System.out.print("-- ");
                }
            }

            System.out.println();
        }

        //Printing the rows + actual grid
        for(int i = 0; i < getRowNum(); i++) {
            //Print rules
            String rule_padding = new String(new char[max_rows_rules - _rowRules[i].size()]).replace("\0", "-- ");
            System.out.print(rule_padding);
            for(Integer rule : _rowRules[i]) {
                String formatted = String.format("%-2d ", rule);
                System.out.print(formatted);
            }
            //Print grid
            for(int col = 0; col < _colNum; col++) {
                System.out.print(_grid[i][col] + "  ");
            }
            System.out.println();
        }
    }
}
